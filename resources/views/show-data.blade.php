@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Импортированные данные</div>

                    <div class="card-body">
                        @foreach ($rows as $date => $dateRows)
                            <h4>{{ $date }}</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя</th>
                                    <th>Дата</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($dateRows as $row)
                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->date->format('d.m.Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endforeach

                        <!-- Pagination links -->
                        <div class="d-flex justify-content-center">
                            {{ $rows->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
