@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Импортировать Excel</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="{{ route('excel.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="excel_file">Файл Excel: </label>
                                <input type="file" class="form-control-file" id="excel_file" name="excel_file">
                            </div>
                            <button type="submit" class="btn btn-primary  mt-3">Импортировать</button>
                        </form>

                            @if ($importProgress > 0)
                                <div class="progress mt-3">
                                    <div class="progress-bar" role="progressbar" style="width: {{ $importProgress }}%;" aria-valuenow="{{ $importProgress }}" aria-valuemin="0" aria-valuemax="100">
                                        {{ $importProgress }}%
                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
