<?php

use App\Http\Controllers\ExcelController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth.basic'])->group(function () {
    Route::get('/', [ExcelController::class, 'index'])->name('excel.index');
    Route::get('/create', [ExcelController::class, 'create'])->name('excel.crete');
    Route::post('/store', [ExcelController::class, 'store'])->name('excel.store');
});
