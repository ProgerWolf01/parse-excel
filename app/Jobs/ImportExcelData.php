<?php
namespace App\Jobs;

use App\Imports\ExcelRowsImport;
use App\Models\Row;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        protected string $filename
    ) {
    }

    public function handle(): void
    {
        $redisKey = 'excel-import-' . $this->filename;
        $array = Excel::toArray(new Row(), $this->filename, 'public',\Maatwebsite\Excel\Excel::XLSX);
        Redis::set($redisKey . '-total', count($array[0]));

        $import = new ExcelRowsImport($this->filename);
        $import->import('public/' . $this->filename);

        if ($import->getErrors()) {
            $errorsString = json_encode($import->getErrors(), JSON_PRETTY_PRINT);
            Storage::put('result.txt', $errorsString);
        }
    }
}
