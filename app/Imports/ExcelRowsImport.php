<?php
namespace App\Imports;

use App\Models\Row;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;

class ExcelRowsImport implements ToModel, WithHeadingRow, WithBatchInserts
{
    use Importable;

    protected int $processedRowsCount = 0;
    protected array $errors = [];

    public function __construct(
        protected ?string $filename = null,
    ) {
    }

    public function model(array $row): Row|null
    {
        $redisKey = 'excel-import-' . $this->filename;
        $this->processedRowsCount++;
        Redis::set($redisKey . '-processed', $this->processedRowsCount);

        $validator = Validator::make($row, $this->rules(), $this->customValidationMessages());

        if ($validator->fails()) {
            $this->errors[] = $validator->errors();
            return null;
        }

        if (!$validator->fails()) {
            $row['date'] = Carbon::createFromFormat('d.m.Y', $row['date'])->format('Y-m-d');

            return new Row([
                'row_id' => $row['id'],
                'name' => $row['name'],
                'date' => $row['date'],
            ]);
        }
    }

    public function rules(): array
    {
        return [
            'id' => 'required|numeric|min:0',
            'name' => 'required|string|regex:/^[a-zA-Z\s]+$/',
            'date' => 'required|date_format:d.m.Y',
        ];
    }

    public function customValidationMessages(): array
    {
        return [
            'id.required' => 'ID is required',
            'id.numeric' => 'ID must be a number',
            'id.min' => 'ID must be a positive number',
            'name.required' => 'Name is required',
            'name.string' => 'Name must be a string',
            'name.regex' => 'Name must contain only letters and spaces',
            'date.required' => 'Date is required',
            'date.date_format' => 'Date must be in the format d.m.Y',
        ];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
