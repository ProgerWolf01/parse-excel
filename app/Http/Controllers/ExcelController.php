<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExcelRequest;
use App\Jobs\ImportExcelData;
use App\Models\Row;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class ExcelController extends Controller
{
    public function index()
    {
        $rows = Row::orderBy('date', 'desc')->paginate(10);

        $groupedRows = $rows->getCollection()->groupBy(function ($item) {
            return $item->date->format('d.m.Y');
        });

        $rows->setCollection($groupedRows);

        return view('show-data', compact('rows'));
    }

    public function create()
    {
        $importProgress = 0;
        $filename = session('filename');

        if ($filename) {
            $redisKey = 'excel-import-' . $filename;
            $processedRows = Redis::get($redisKey . '-processed');
            $totalRows = Redis::get($redisKey . '-total');

            if ($totalRows) {
                $importProgress = round(($processedRows / $totalRows) * 100, 2);
            }
        }

        return view('excel-import', compact('importProgress'));
    }

    public function store(CreateExcelRequest $request)
    {
        $file = $request->file('excel_file');
        $filename = $file->getClientOriginalName();
        $file->storeAs('public', $filename);

        $redisKey = 'excel-import-' . $filename;
        Redis::set($redisKey . '-processed', 0);
        Redis::set($redisKey . '-total', 0);

        session(['filename' => $filename]);

        ImportExcelData::dispatch($filename)
            ->onQueue('excel-import');

        return redirect()->back()->with('success', 'Excel file uploaded and import process started.');
    }
}
