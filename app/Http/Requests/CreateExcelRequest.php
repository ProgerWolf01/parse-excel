<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateExcelRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'excel_file' => 'required|file|mimes:xlsx',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
