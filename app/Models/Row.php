<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Row extends Model
{
    protected $fillable = [
        'row_id',
        'name',
        'date',
    ];

    protected function casts()
    {
        return [
            'date' => 'date',
        ];
    }
}
